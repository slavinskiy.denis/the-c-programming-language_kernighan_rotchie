#include <stdio.h>
#define START 300
#define STEP 20
#define END 0

/*
 * Вывод таблицы температур по Фаренгейту и Цельсию
 * для fahr = 0, 20, ..., 300.
 */
int main() {
    int fahr;

    printf("+------+---------+\n");
    printf("| Fahr | Celsius |\n");
    printf("+------+---------+\n");

    for (fahr = START; fahr >= END; fahr -= STEP) {
        printf("| %4d | %7.1f |\n", fahr, (5.0 / 9) * (fahr - 32));
    }

    printf("+------+---------+\n");
}
