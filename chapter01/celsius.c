#include <stdio.h>

/*********************************
 *       Celsius task 1.4        *
 ********************************/
int main() {
    int start = 0;
    int end = 100;
    int step = 5;

    float fahr, celsius;

    printf("+-------+-------+\n");
    printf("| Cels  |  Fahr |\n");
    printf("+-------+-------+\n");
    celsius = start;
    while (celsius <= end) {
        fahr = celsius * (9.0/5.0) + 32;
        printf("| %5.0f | %5.1f |\n", celsius, fahr);
        celsius += step;
    }
    printf("+-------+-------+\n");
}
